<!DOCTYPE html>
<br>
<br>
<ul class="nav nav-tabs">
	<li><a href="http://localhost/auth/my_page">Моя страница</a></li>
	<li><a href="#">Сообщения</a></li>
	<li class="active"><a href="http://localhost/auth/my_photos?id=<?php echo $result['id'];?>">Мои фотографии</a></li>
	<li><a href="#">Друзья</a></li>
  	<li><a href="#">Новости</a></li>
  	<li><a href="http://localhost/auth/index">Выход</a></li>	
</ul>
<html>

	<head>
		<meta charset="utf-8"/>
		<title>Фото</title>

		<!-- Google web fonts -->
		<link href="http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700" rel='stylesheet' />

		<!-- The main CSS file -->
		<link href="assets/css/style.css" rel="stylesheet" />
	</head>

	<body>
		<form id="upload" method="post" action="upload.php" enctype="multipart/form-data">
			<div id="drop">
				Перетащите сюда

				<a>Обзор</a>
				<input type="file" name="upl" multiple />
			</div>

			<ul>
				<!-- The file uploads will be shown here -->
			</ul>

		</form>  
		<!-- JavaScript Includes -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="assets/js/jquery.knob.js"></script>
		<!-- jQuery File Upload Dependencies -->
		<script src="assets/js/jquery.ui.widget.js"></script>
		<script src="assets/js/jquery.iframe-transport.js"></script>
		<script src="assets/js/jquery.fileupload.js"></script>
		<!-- Our main JS file -->
		<script src="assets/js/script.js"></script>
	</body>
</html>