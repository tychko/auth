<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
<title>Авторизация пользователя</title>
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/mask.js"></script>
<link rel="stylesheet" href = "css/style.css">
<link rel="stylesheet" href = "css/normalize.css">
<script src="js/callServer.js"></script>
</head>
<body>
		<div class="head">
			<a href="index.php"><span style="color:'white';" onmouseover="this.style.color='green';" onmouseout="this.style.color='white';">Главная</span></a>
			<a href="#"><span style="color:'white';" onmouseover="this.style.color='green';" onmouseout="this.style.color='white';">Только для администраторов</span></a>
			<a href="#dialog" name="modal">Вход</a>
			<a href="#dialog1" name="modal">Регистрация</a>	
		</div>	
		<div class="content">
		<div class="it_people">
		</div>
		<div id="boxes">
  			<div id="dialog" class="window">
  				<div class ="text">Авторизируйтесь, чтобы прочитать содержимое этой страницы!</div><br>
				<div class="login_pass">
					<input type="text" placeholder="Login" name="login" id="login"><br>
					<input type="password" placeholder="Pass" name="pass" id="pass"><br><br>
						<div class="submit">
							<input type="button" value="Войти" onclick="callServer()">
						</div>
					<div id="resultDiv"></div>
				</div>
      		</div>
      	</div>	
      	<div id="boxes">	
  			<div id="dialog1" class="window">
  				<div class ="text">Введите логин и пароль</div><br>
						<div class="login_pass">
							<input type="text" placeholder="Login"  id="login_reg"><br>
							<input type="password" placeholder="Password" id="pass_reg"><br><br>
							<div class="submit">
								<input type="button" value="Зарегестрироваться" onclick="callServerReg()">
							</div>
						<div id="resultDiv_reg"></div>
						</div>
  				</div>
			</div>
		</div>	
		<div id="mask"></div>
	</div>
	<div class="bottom">&copy Tychko O.S. 2014
	</div>
</body>
</html>
