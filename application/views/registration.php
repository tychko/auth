<div class ="text">Введите Ваши данные!</div><br>
<form class="form-horizontal" method="POST" action="http://localhost/auth/registration" enctype="multipart/form-data">
 	<div class="form-group">
    	<label class="col-sm-2 control-label">Email</label>
    	<div class="col-sm-10">
      		<input class="form-control" type="text" placeholder="Email" name="email">
    	</div>
  	</div>
    <div class="form-group">
    	<label class="col-sm-2 control-label">Login</label>
   		<div class="col-sm-10">
      		<input class="form-control" type="text" placeholder="Login" name="login">
    	</div>
  	</div>
  	<div class="form-group">
    	<label  class="col-sm-2 control-label">Пароль</label>
    	<div class="col-sm-10">
      		<input type="password" class="form-control" placeholder="Password" name="password">
    	</div>
  	</div>
    <div class="form-group">
    	<label class="col-sm-2 control-label">Имя</label>
    	<div class="col-sm-10">
      		<input class="form-control" type="text" placeholder="Ваше Имя" name="name">
    	</div>
  	</div>
    <div class="form-group">
    	<label class="col-sm-2 control-label">Фамилия</label>
    	<div class="col-sm-10">
      		<input class="form-control" type="text" placeholder="Ваша Фамилия" name="surname">
    	</div>
  	</div>
  	<div class="form-group">
    	<label class="col-sm-2 control-label">Ваше фото</label>
    	<div class="col-sm-10">
    		<input type="hidden" name="MAX_FILE_SIZE" value="1000000">
      		<input type="file" name="user_file">
      		<p class="help-block">Размер не должен привышать 1МБ</p>
    	</div>
  	</div>
  	<div class="form-group">
    	<div class="col-sm-offset-2 col-sm-10">
      	<input class="btn btn-info" type="submit" value="Зарегестрироваться">
    	</div>
  	</div>
</form> 
<div class="visible-lg">
  <div class="it_people">
  </div>
</div> 
