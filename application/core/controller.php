<?php
class Controller {
	
	public $model;
	public $view;

	function __construct()
	{
		$modelName =  'Model_' . str_replace('Controller_','',get_class($this));
		$this-> model = new $modelName;
		$this -> view = new view();
	}
}