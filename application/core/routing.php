<?php
class Routing
{
	static function execute() 
	{
		$controllerName = 'Main';	
		$actionName = 'index';

		$piecesOfUrl = explode('/', $_SERVER['REQUEST_URI']);
		
		if (!empty($piecesOfUrl[1])) 
		{
			$controllerName = $piecesOfUrl[1];

		}

		if (!empty($piecesOfUrl[2]))
		{
			$action = $piecesOfUrl[2];
			$action = explode('?', $action);
			$actionName = $action[0];
		}

		$modelName = 'Model_' . $controllerName;
		$controllerName = 'Controller_' . $controllerName;
		$actionName = 'action_' . $actionName;

		$fileWithModel = strtolower($modelName) . '.php';
		$fileWithModelPath	= "application/models/" . $fileWithModel;

		if (file_exists($fileWithModelPath)){
			include $fileWithModelPath;
		}

		$fileWithController = strtolower($controllerName) . '.php';
		$fileWithControllerPath = "application/controllers/" . $fileWithController;

		if (file_exists($fileWithControllerPath)){
			include $fileWithControllerPath;
		}
		else{
			$fileWithControllerPath = "application/views/404.php";
			include $fileWithControllerPath;
		}
		$controller = new $controllerName;
		$action = $actionName;

		if (method_exists($controller, $action)){
			call_user_func(array($controller, $actionName), $piecesOfUrl);
		}
		else{
			$fileWithControllerPath = "application/views/404.php";
			include $fileWithControllerPath;
		}
	}	
}



