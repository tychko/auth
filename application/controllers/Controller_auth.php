<?php
class Controller_auth extends Controller
{
	public function action_index()
	{
		$this -> view -> make('index.php', 'main_template_view.php',$result);	
	}
	public function action_registration_page()
	{
		$this-> view-> make('registration.php', 'main_template_view.php',$result);	
	}
	public function action_authorization_page()
	{
		$this-> view-> make('authorization.php', 'main_template_view.php',$result);	
	}
	public function action_authorization()
	{
		if (empty($_POST['login'])) {
			die('Введите логин');
		}
		if (empty($_POST['password'])) {
			die('Введите пароль');
		}
		$login = $_POST["login"];
		$password = $_POST["password"];
			$id = $this-> model->authorization($login, $password);
			$result = $this-> model->get_user_page($id);
			$this-> view ->make('my_page.php', 'main_template_view.php',$result);	

	}
	public function action_registration()
	{
		if (empty($_POST['email'])) {
			die('Введите Email');
		}
		if (empty($_POST['login'])) {
			die('Введите логин');
		}
		if (empty($_POST['password'])) {
			die('Введите пароль');
		}
		if (empty($_POST['name'])) {
			die('Введите Ваше имя');
		}
		if (empty($_POST['surname'])) {
			die('Введите Вашу фамилию');
		}
		if($_FILES['user_file']['size']>1000000)
			die('Ошибка. Размер файла больше чем 1МБ');
		else{
			$this-> model->add_user();
		}
		$this-> view ->make('authorization.php', 'main_template_view.php',$result);
	}
	public function action_my_page()
	{
		$result = $this-> model->get_user_page();
		$this-> view ->make('my_page.php', 'main_template_view.php',$result);	
	}
	public function action_my_photos()
	{
		$result = $this-> model->get_user_photos();
		$this -> view -> make('my_photo.php', 'main_template_view.php',$result);	

	}

}
?>