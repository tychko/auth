<?php
ini_set('display_errors', 1);

require_once 'application/core/routing.php';
require_once 'application/core/model.php';
require_once 'application/core/view.php';
require_once 'application/core/controller.php';
require_once 'application/load.php';

