function callServer()
{
	var login = document.getElementById('login').value;
	var pass = document.getElementById('pass').value;
	var httpRequest = new XMLHttpRequest();
	httpRequest.open('POST', 'auth.php', true);
    httpRequest.setRequestHeader('Content-Type',  'application/x-www-form-urlencoded');
   	httpRequest.onreadystatechange = function()
	{
		if (httpRequest.readyState == 4)
		{
			var div = document.getElementById('resultDiv');
			div.innerHTML = httpRequest.responseText;
		}
	}
	httpRequest.send('login='+login+'&'+'pass='+pass);
}
function callServerReg()
{
	var login = document.getElementById('login_reg').value;
	var pass = document.getElementById('pass_reg').value;
	var httpRequest = new XMLHttpRequest();
	httpRequest.open('POST', 'reg.php', true);
    httpRequest.setRequestHeader('Content-Type',  'application/x-www-form-urlencoded');
   	httpRequest.onreadystatechange = function()
	{
		if (httpRequest.readyState == 4)
		{
			var div = document.getElementById('resultDiv_reg');
			div.innerHTML = httpRequest.responseText;
		}
	}
	httpRequest.send('login_reg='+login+'&'+'pass_reg='+pass);
}